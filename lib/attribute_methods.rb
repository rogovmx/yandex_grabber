module ActiveRecord
  # = Active Record Attribute Methods
  module AttributeMethods
    module ClassMethods

      # A method name is 'dangerous' if it is already (re)defined by Active Record, but
      # not by any ancestors. (So 'puts' is not dangerous but 'save' is.)
      def dangerous_attribute_method?(name) # :nodoc:
        false
#        method_defined_within?(name, Base)
      end
    end
  end
end
